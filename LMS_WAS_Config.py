#*********************************************************************************************************************************#
#	Product     :	Intellect
#   Description :   Jython Script for Webshphere Automated Installation Deployment for Intellect Applicatoin	
#
#	Date		Version		Author						Description
#	15-04-2014	1			Sachin Patil			Initial draft for creating JMS, JDBC and Shared libraries for LMS
#	30-11-2017  2			Prisha Haria		    SUP_ISS_I_8590	changed datasource to xa-datasource
#**********************************************************************************************************************************#


"""
-- USAGE ----
Use the below command for running script 

-- for non-secure environment
<WAS_INSTALLED_PATH>/wsadmin.sh -lang jython -f <Script Name>

- For security enabled environment
<WAS_INSTALLED_PATH>/wsadmin.sh -lang jython -user <WAS User> -password <Password> -f <Script Name> <Script Options>

see the usage for script options
"""


"""
Jython reference link
http://www.jython.org/jythonbook/en/1.0/index.html
http://www.webspheretools.com/sites/webspheretools.nsf/docs/WebSphere%20Jython%20JMS%20Scripts!opendocument
"""


#from __future__ import nested_scopes

# * Import java classes 
import sys
from types import *
from java import lang
from java import util
from java import io
from java.io import File
import time


lineSeparator = java.lang.System.getProperty('line.separator')


# -- Initializing globle variables -----------
nbrDashInLine = 80
ScriptName =  "LMS_WAS_Config"

# Added by Sachin for logger file name with date time
#CurrDateTime = time.strftime("%Y%m%d_%H%M", time.localtime())
#handle = open(ScriptName+'_'+CurrDateTime+'.log',"a")

handle = open(ScriptName+'.log',"a")
# ------
logline = []

# -- End of Initializing globle variables -----------


# -- Start of all function Definitions ------------------------------------------------------------------------------------------------

#================================
# Generic function definitions
#================================


# For printing usage
def usage():
	Logger("-" * nbrDashInLine)
	Logger( "Usage:" )
	Logger( "	LMS_WAS_AppConfig.py [HELP] [SHAREDLIB] [JMS] [JDBC] [DEPLOY <AppName>] [UNDEPLOY <AppName>] [STOP <AppName>] [START <AppName>] [RESTART <AppName>]" )
	Logger("")
	Logger( "e.g. ..... " )
	Logger( "	LMS_WAS_AppConfig.py JMS" )
	Logger( "		For creating JMS configuration" )
	Logger( "	LMS_WAS_AppConfig.py JDBC" )
	Logger( "		For creating JDBC (Datasource) configuration" )
	Logger("	")
	Logger( "	LMS_WAS_AppConfig.py DEPLOY <app name>" )
	Logger( "		For deploying applications" )
	Logger( "	LMS_WAS_AppConfig.py UNDEPLOY <app name>" )
	Logger( "		For undeploying application specified in <app name>" )
	Logger("	")
	Logger( "	LMS_WAS_AppConfig.py STOP <app name>" )
	Logger( "		For Stopping applications " )
	Logger( "	LMS_WAS_AppConfig.py START <app name>" )
	Logger( "		For Starting applications " )
	Logger( "	LMS_WAS_AppConfig.py RESTART <app name>" )
	Logger( "		For Restarting applications " )
	Logger("	")
	Logger( "	LMS_WAS_AppConfig.py JMS JDBC" )
	Logger( "		For creating JMS and JDBC configuration one by one" )
	Logger("-" * nbrDashInLine)
#end-def

# * Load propperty file defination
def loadProps (source):
	result = {}
	if type(source) == type(''):
		source = io.FileInputStream(source)
		bis = io.BufferedInputStream(source)
		props = util.Properties()
		props.load(bis)
		bis.close()
		for key in props.keySet().iterator():
			result[key] = props.get(key)
	return result
# endDef


# * Logging into the log file
def Logger(logValue, default='Print'):
	logline = []
	DateValue = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
	logline.append(DateValue+" : "+logValue)
	handle.write((" ".join(logline))+"\n")
	if default == 'Print':
		print logValue
	return 
# endDef

def getInput(pInputMsg):
	Logger("\n")
	IU = raw_input(pInputMsg)
	Logger(pInputMsg+IU, 'NoPrint')
	return IU
# End def

# * Saving to Master configuration.
def SaveConfiguration():
	Logger("")
	Logger("-" * 40)
	Logger("Saving Master configuration...")
	AdminConfig.save()
	if isCluster == 'true':
		AdminNodeManagement.syncActiveNodes()
	Logger("-" * 40)
	Logger("")
# endDef of SaveConfiguration()


# -- End of Generic function definitions -------------------------



# -- App Configuration function definitions ------------------------------

# For creating Shared Libraries
def CreateSharedLibraries():
	# * Configuring a shared library
	Logger("\n")
	Logger("CREATION OF SHARED LIBRARY STARTED")
	Logger("")

	# This is the home path of all the libraries which will be prefixed to each component of all the libraries given below
	lSharedLib_HomePath = mLMSProperties.get('SharedLib_HomePath','0')

	lSharedLibs = mLMSProperties.get('LMS_SharedLibs')
	lSharedLibs = eval(lSharedLibs)

	
	# Loop to create shared library using above data structure.
	for tmpSharedLibDef in lSharedLibs:
		lLibName = tmpSharedLibDef[0]
		lLibItemList = tmpSharedLibDef[1]
		Logger("Checking Library '"+lLibName+"'")
		tmpSharedLibId = AdminConfig.getid(mScopeStr+'/Library:'+lLibName+'/')
		lenSharedLibId = len(tmpSharedLibId); 

		if(lenSharedLibId > 0):
			Logger("Removing existing Referance Library...")
			#Logger(tmpSharedLibId)
			AdminConfig.remove(tmpSharedLibId)
			Logger("Referance Library Removed")
		
		# Collecting all the lib items.
		lSharedLibValue = ""
		for tmpLibItem in lLibItemList:
			lSharedLibValue += lSharedLib_HomePath+tmpLibItem+";"
		
		Logger("Creating Shared Library '"+ lLibName +"' with following value")
		Logger("'"+ lSharedLibValue +"'")
		AdminConfig.create('Library',mScopeId,[['name', lLibName],['classPath', lSharedLibValue ]])
		Logger("Referance Library "+lLibName+" created sucessfully")
		Logger("")
		
	# End for loop
	
	return

# endDef of CreateSharedLibraries


def CreateJMS_Config():
	
	Logger("")
	Logger("CREATION OF JMS Configuration STARTED for (SIBus, QCF/TCF, Q/Topic, Bus Destination and Activation Specification)...")
	Logger("")
	
	lJMS_JNDI_Prefix = mLMSProperties.get("JMS.JNDI.Prefix",'')
	
	JMSDefList = mLMSProperties.get('JMSDefList')
	JMSDefList = eval(JMSDefList)

	# Collecting QCF attributes from properties
	QCF_Attributes = mLMSProperties.get('QCF.Attributes')
	#print "QCF_Attributes = "+QCF_Attributes
	# This will evaluate string from a property to a dictionary type
	QCF_Attributes = eval(QCF_Attributes)


	#print JMSDefList
	
	for JMSDefItem in JMSDefList:
		
		# --- SIBus Configuration --------------------------
		# getting SIBus name from the JMSDefList
		lPropSIBusName = JMSDefItem[0]
		
		# Prefixing SIBus name with server or cluster name (Bus will be created by this name) 
		lSIBusName = mPrefixScopeStr+"_"+lPropSIBusName
		
		# Checking for SIBus existance
		Logger("Checking SIBus ["+lSIBusName+"] ...")
		lSIBusID = AdminConfig.getid('/SIBus:%s' % lSIBusName)

		if((len(lSIBusID) == 1) or (len(lSIBusID) == 0)):
			# Creating SIBus as it doesn't exists ....
			Logger("Creating "+lSIBusName+" as it doesn't exists")
			lSIBusParams = ["-bus", lSIBusName, "-description", lSIBusName]
			# Use deprecated -secure option for compatibility with prior versions of this script.
			#lSIBusParams.append("-secure")
			#lSIBusParams.append("FALSE")
			AdminTask.createSIBus(lSIBusParams)
			Logger("")
			Logger("<<< "+lSIBusName+" >>> created successfully")
			Logger("")
			# End if creating SIBus....
			
			# Adding bus-member to above created bus (as a cluster or node-server depending on the scope)....
			Logger("Adding members to SIBus ["+lSIBusName+"]...")
			lSIBusMemberParams = ["-bus", lSIBusName]
			
			lSIBusMemberParams.extend(mScopeList)
			lSIBusMemberParams.extend(["-createDefaultDatasource", mSIBDefaultDS])
			
			if mSIBDefaultDS == 'false':
				lDS_JNDIName = mLMSProperties.get(lPropSIBusName+'_DS_JNDIName','0')
				lDS_SchemaName = mLMSProperties.get(lPropSIBusName+'_DS_SchemaName','0')
				if (lDS_JNDIName  != '0'):
					lSIBusMemberParams.extend([
						"-datasourceJndiName", lDS_JNDIName,
						"-schemaName", lDS_SchemaName
					])
				else:
					lErrorMessage = "For Cluster scope Data source and Schema are not defined in property file"
					Logger(lErrorMessage)
					sys.exit("Aborted : "+lErrorMessage)
				#end if-else
			#endif

			#print lSIBusMemberParams
			AdminTask.addSIBusMember(lSIBusMemberParams)
			Logger("Member Added successfully")
			Logger("")
			# Bus member addition completed.
			
		else:
			Logger("!!! "+lSIBusName+" !!! already exists with ID ["+lSIBusID+"]")
		#end-if-Else
		# --- End of SIBus configuration --------------------------

		Logger("\n------------\n")

		# --- Start of Connection Factory configuration --------------------------
		lCFList = JMSDefItem[1] # getting JMS CF Definitions as a list of dictionaries 
		
		for CFItems in lCFList:
			
			CFType = CFItems.get('TYPE')
			#CFItems = CFItems.split(':')
			if CFType == 'QCF': CFType = "Queue" 
			else: CFType = "Topic"
			CFName = CFItems.get('NAME')
			Logger("Checking "+CFType+" Connection Factory ["+CFName+"]")
			CFLevel = CFItems.get('LEVEL')
			if CFLevel == 'CELL':
				mScopeStrForCF = '/Cell:'+mCell
				mScopeIdForCF = AdminConfig.getid(mScopeStrForCF)
			else:
				mScopeStrForCF = mScopeStr
				mScopeIdForCF = mScopeId
			#end if
			tmpCF_ID = AdminConfig.getid(mScopeStrForCF+'/J2CResourceAdapter:SIB JMS Resource Adapter/J2CConnectionFactory:'+CFName)
			if (len(tmpCF_ID) != 0):
				Logger("")
				Logger("!!! '"+CFName+"' !!! JMS "+CFType+" connection factory already exists.")
			else:
				Logger("Creating '"+CFName+"' as it doesn't exist.")
				lCF_Params = ["-name", CFName, "-jndiName", lJMS_JNDI_Prefix+CFName, "-description", CFName, "-type", CFType, "-busName", lSIBusName]
				# Add atrribute for QCF here
				tmpQCF_Attribs = QCF_Attributes.get(CFName)
				if tmpQCF_Attribs != None:
					lCF_Params.extend(tmpQCF_Attribs)
				AdminTask.createSIBJMSConnectionFactory(mScopeIdForCF, lCF_Params)
				Logger("")
				Logger("<<< '"+CFName+"' >>> created successfully on scope (" +mScopeStrForCF+ ")")
			#endIf
			Logger("")
		# --- End of Connection Factory configuration --------------------------

		Logger("\n------------\n")

		
		
		# --- Creating Q, Topic, Bus Destination and Activation Specification --------------------------
		# Getting new JMS definition list (Q, Topic, BD and AS)
		lJMSDefList = JMSDefItem[2] # getting JMSDefinitions (Q/Topic name, BD name and AS name) as a list
		for JMSDefItems in lJMSDefList:
			Logger("-- SIBus:"+lPropSIBusName+" --")
			
			lJMSType = JMSDefItems[0] # first element is a flag which determines current processing list contains Q or Topic details
			if lJMSType == "Q" :
				lJMSTypeName = "Queue"
			else:
				lJMSTypeName = "Topic"
			# endif

			Logger("")
			Logger("Checking/Creating "+lJMSTypeName+" configuration : ["+(", ".join(JMSDefItems))+"]")
			Logger("")
			
			# Collecting Q/Topic name, BD name and AS name
			lJMSDefName = JMSDefItems[1]
			lJMSDefBDName = JMSDefItems[2]
			lJMSDefASName = JMSDefItems[3]
			lJMSDefScope = 'default'
			if len(JMSDefItems) == 5 :
				lJMSDefScope = JMSDefItems[4]
			#end if
			if lJMSDefScope == 'CELL':
				mScopeStrForCF = '/Cell:'+mCell
				mScopeIdForCF = AdminConfig.getid(mScopeStrForCF)
			else:
				mScopeStrForCF = mScopeStr
				mScopeIdForCF = mScopeId
			#end if

			# Start of Q/Topic configuration
			Logger("Checking for "+lJMSTypeName+" ["+lJMSDefName+"]...")
			isJMSDefExist = 'false'
			# collecting existing list from given scope
			if lJMSType == "Q" :
				lJMSDefBD_Option = ["-queueName", lJMSDefBDName]
				lExistingJMSDefList = AdminTask.listSIBJMSQueues(mScopeIdForCF).splitlines()
			else:
				lJMSDefBD_Option = ["-topicName", lJMSDefBDName]
				lExistingJMSDefList = AdminTask.listSIBJMSTopics(mScopeIdForCF).splitlines()
			# end-if
			for tmpJMSDefItem in lExistingJMSDefList:
				tmpJMSDefName = AdminConfig.showAttribute(tmpJMSDefItem, "name")
				if (tmpJMSDefName == lJMSDefName):
					isJMSDefExist = 'true'
					Logger("")
					Logger("!!! '"+lJMSDefName+"' !!! This SIB JMS "+lJMSTypeName+" already exists.")
					break # breaking for loop
				# enf if
			# end for loop
			if isJMSDefExist == 'false':
				Logger("Creating "+lJMSTypeName+" '"+lJMSDefName+"' as it doesn't exist.")
				lJMSDefParams = ["-name", lJMSDefName, "-jndiName", lJMS_JNDI_Prefix+lJMSDefName, "-description", lJMSDefName, "-busName", lSIBusName]
				lJMSDefParams.extend(lJMSDefBD_Option)
				#print lJMSDefParams
				if lJMSType == "Q" :
					AdminTask.createSIBJMSQueue(mScopeIdForCF, lJMSDefParams)
				else:
					AdminTask.createSIBJMSTopic(mScopeIdForCF, lJMSDefParams)
				Logger("")
				Logger("<<< '"+lJMSDefName+"' >>> "+lJMSTypeName+" created successfully")
			# end if
			Logger("")
			# end of Q/Topic configuration 
			
			# Start of Bus Destination configuration
			Logger("Checking for "+lJMSTypeName+"BD ["+lJMSDefBDName+"]...")
			isJMSDefBDExist = 'false'
			msgThreshold = '50000'
			# collecting existing list from given scope
			if lJMSType == "Q" :
				lJMSDefBDType = "Queue"
				lExistingJMSDefBDList = AdminTask.listSIBDestinations("-bus "+lSIBusName).splitlines()
			else:
				lJMSDefBDType = "TopicSpace"
				lExistingJMSDefBDList = AdminConfig.list("SIBTopicSpace").splitlines()
			# end-if
			for tmpJMSDefBDItem in lExistingJMSDefBDList:
				tmpJMSDefBDName = AdminConfig.showAttribute(tmpJMSDefBDItem, "identifier")
				if (tmpJMSDefBDName == lJMSDefBDName):
					isJMSDefBDExist = 'true'
					Logger("")
					Logger("!!! '"+lJMSDefBDName+"' !!! This "+lJMSTypeName+" Bus Destination already exists.")
					break
				# enf if
			# end for loop
			if isJMSDefBDExist == 'false':
				Logger("Creating '"+lJMSDefBDName+"' as it doesn't exist.")
				if lJMSDefBDName == 'SacTestQBD':
					msgThreshold = '1'
					# modify SIBEngine to set ["-highMessageThreshold", msgThreshold]
					# and reset this after creating SIBDestination.
				lJMSDefBDParams =  ["-bus", lSIBusName, "-name", lJMSDefBDName, "-type", lJMSDefBDType]
				lJMSDefBDParams.extend(mScopeList)
				AdminTask.createSIBDestination(lJMSDefBDParams)
				Logger("")
				Logger("<<< "+lJMSDefBDName+" >>> "+lJMSTypeName+" Bus Destination created successfully")
			# end if
			Logger("")
			# end of Bus Destination configuration 
			
			# Start of Activation Specification configuration
			Logger("Checking for "+lJMSTypeName+"AS ["+lJMSDefASName+"]...")
			if (lJMSDefASName.lower() == 'null' or len(lJMSDefASName) == 0):
				Logger("Ignoring "+lJMSTypeName+"AS creation, as NULL or blank value found for given property.")
			else:
				isJMSDefASExist = 'false'
				# collecting existing list from given scope
				lExistingJMSDefASList = AdminTask.listSIBJMSActivationSpecs(mScopeIdForCF).splitlines()
				for tmpJMSDefASItem in lExistingJMSDefASList:
					tmpJMSDefASName = AdminConfig.showAttribute(tmpJMSDefASItem, "name")
					if (tmpJMSDefASName == lJMSDefASName):
						isJMSDefASExist = 'true'
						Logger("")
						Logger("!!! '"+lJMSDefASName+"' !!! This "+lJMSTypeName+" Activation Specification already exists.")
						break
					# enf if
				# end for loop
				if isJMSDefASExist == 'false':
					Logger("Creating '"+lJMSDefASName+"' as it doesn't exist.")
					lJMSDefASParams =  ["-name", lJMSDefASName, "-jndiName", lJMS_JNDI_Prefix+lJMSDefASName, "-busName", lSIBusName, "-destinationJndiName", lJMSDefName, "-destinationType", lJMSTypeName]
					AdminTask.createSIBJMSActivationSpec(mScopeIdForCF, lJMSDefASParams)
					Logger("")
					Logger("<<< "+lJMSDefASName+" >>> "+lJMSTypeName+" Activation Specification created successfully")
				# end if
			# end if-else
			Logger("")
			# end of Activation Specification configuration 
			
			Logger("")
			Logger("Finished "+lJMSTypeName+" configuration : ["+(", ".join(JMSDefItems))+"]")
			Logger("\n***********\n")
		# end of for loop 
		
		Logger("\n------------\n")
	return
# end-Def of CreateJMS_Config()




def CreateJDBC_Config():
	
	Logger("")
	Logger("Creating LMS JDBC Configuration ...")
	Logger("")

	# Start JDBC Configuration
	lJDBC_JNDI_Prefix = mLMSProperties.get("JDBC.JNDI.Prefix",'')
	Logger("JDBC_JNDI_Prefix = "+lJDBC_JNDI_Prefix)

	# Checking/Creating JDBC Provider
	lProviderName = ['name', 'Oracle JDBC Driver (XA)']
	Logger("")
	Logger("Checking/Creating JDBC Provider ["+lProviderName[1]+"]...")
	Logger("")
	lJdbcProviderId = AdminConfig.getid(mScopeStr+'/JDBCProvider:'+lProviderName[1]+'/')
	if len(lJdbcProviderId) == 0:
		# Set the required attributes for a Oracle JDBCProvider.
		lProviderDescription = ['description', lProviderName[1]]
		lProviderImplClassName = ['implementationClassName', 'oracle.jdbc.xa.client.OracleXADataSource']
		JDBC_CLS_Path = mLMSProperties.get('JDBC_CLS_Path','0')
		lProviderClasspath = ['classpath', JDBC_CLS_Path]
		lProviderJdbcAttrs = [lProviderName, lProviderImplClassName, lProviderDescription, lProviderClasspath]
		#Logger("lProviderJdbcAttrs = "+lProviderJdbcAttrs)
		# Create the JDBC Provider
		lJdbcProviderId = AdminConfig.create('JDBCProvider', mScopeId, lProviderJdbcAttrs)
		Logger("JDBC Provider <<< '"+lProviderName[1]+"' >>> created successfully")
	else:
		Logger("JDBC Provider !!! '"+lProviderName[1]+"' !!! already exists")
		#Logger("JDBC ProviderId: "+lJdbcProviderId)
		
	Logger("")
	Logger("End of Checking/Creating JDBC Provider ...")
	Logger("")

	# Collecting Datasources
	JDBC_DS_LIST = mLMSProperties.get('JDBC.DS.LIST','[]')
	JDBC_DS_LIST = eval(JDBC_DS_LIST)
	DS_Count = len(JDBC_DS_LIST)
	Logger("DataSource_Count = "+str(DS_Count))
	DS_Count = 1
	for DSData in JDBC_DS_LIST :
		Logger("")
		Logger("-- DS # "+str(DS_Count)+" --")
		lDS_Name = DSData.get("DatasourceName", "none")
		
		lDS_SchemaName = DSData.get("SchemaName","none")
		lDS_SchemaPassword = DSData.get("Password",'')
		lDS_AuthDataAlias = DSData.get("AuthDataAlias","none")
		lDS_DB_IP = DSData.get("DB_IP","none")
		lDS_DB_Port = DSData.get("DB_Port","none")
		lDS_DB_SID = DSData.get("DB_SID","none")
		lDS_DB_StatementCacheSize = DSData.get("StatementCacheSize","none")
		lDSConnPoolAttrs = DSData.get("DSConnPoolAttrs",'')

		Logger("")
		Logger("Checking/Creating JDBC DataSource [ "+lDS_Name+" ]...")
		Logger("")
		tmpDataSource = AdminConfig.getid(mScopeStr+'/JDBCProvider:'+lProviderName[1]+'/DataSource:'+lDS_Name+'/')
		if len(tmpDataSource) == 0:
			Logger("Creating DataSource '"+lDS_Name+"' as it doesn't exist.")
			lDsAttributes = [
				['name', lDS_Name],
				['jndiName', lJDBC_JNDI_Prefix+lDS_Name],
				['description', lDS_Name],
				['datasourceHelperClassname', 'com.ibm.websphere.rsadapter.Oracle11gDataStoreHelper'],
				["statementCacheSize", lDS_DB_StatementCacheSize]
			]
			if len(lDS_SchemaPassword) == 0:
				lDsAttributes.append(["authDataAlias" , lDS_AuthDataAlias])
			
			lNewDSId = AdminConfig.create('DataSource', lJdbcProviderId, lDsAttributes)
			Logger("")
			Logger("<<< "+lDS_Name+" >>> Data Source created successfully")
			Logger("")
			
			# --- Creating Connection Pool Setting for this DS
			Logger("Creating Connection Pool Setting for this DataSource")
			AdminConfig.create("ConnectionPool", lNewDSId , lDSConnPoolAttrs)

			# --- Adding custom properties to this DS
			Logger("")
			Logger("Adding following custom properties to this DataSource")
			Logger("")
			lDSResPropSet = AdminConfig.create('J2EEResourcePropertySet' , lNewDSId , "")
			
			# Set Property : URL 
			JDBC_URL = "jdbc:oracle:thin:@"+lDS_DB_IP+":"+lDS_DB_Port+"/"+lDS_DB_SID
			Logger(".... Adding DS Property : URL [ "+JDBC_URL+" ]")
			lDSProperty = [
				["name",		"URL"],
				["type",		"java.lang.String"],
				["required",	"true"],
				["value",		JDBC_URL]
			]
			AdminConfig.create('J2EEResourceProperty' , lDSResPropSet , lDSProperty)
			
			if len(lDS_SchemaPassword) != 0:
				# Set Property : user 
				Logger(".... Adding DS Property : user [ "+lDS_SchemaName+" ]")
				lDSProperty = [
					["name" , "user"],
					["type" , "java.lang.String"],
					["value", lDS_SchemaName]
				]
				AdminConfig.create('J2EEResourceProperty' , lDSResPropSet , lDSProperty)
				
				# Set Property : password 
				Logger(".... Adding DS Property : password [ *********** ]")
				lDSProperty = [
					["name" , "password"],
					["type" , "java.lang.String"],
					["value", lDS_SchemaPassword]
				]
				AdminConfig.create('J2EEResourceProperty' , lDSResPropSet , lDSProperty)
			#end-if
		else:
			Logger("DataSource !!! '"+lDS_Name+"' !!! already exists")
		# end-if-else
		
		DS_Count = DS_Count + 1
		Logger("") 
	# end-for
	
	Logger("-" * 40) 
	Logger("End of JDBC Configuration") 
	Logger("-" * 40) 
	# End of JDBC Configuration
	
	# -- final defination return statement ----
	return
# end-Def of CreateJDBC_Config



# --- Functions for deploy/undeploy application ----------------------------

def isApplicationExist(AppName):
	Logger("Checking for existance of an applciation "+AppName+" ...")
	
	if isManagedProcess == 'true':
		appList = AdminApp.list(mAppMgrScopeStr).replace("\r", "").split("\n")
	else:
		appList = AdminApp.list().replace("\r", "").split("\n")
	
	#Logger("Installed AppList = "+str(appList))
	isExist="NO"
	for tmpApp in appList :
		if tmpApp == AppName :
			isExist = "YES"
	#end-for
	if isExist == "YES" :
		Logger("Applciation exist")
	else:
		Logger("Applciation does not exist")
	#end-if-else
	return isExist
#end-def isApplicationExist

# Uninstall Application
def unDeployApplication(AppName):
	appExistLevel = isApplicationExist(AppName)
	if(appExistLevel == "YES"):
		Logger("Undeploying applciation " , AppName)
		AdminApp.uninstall(AppName)
		AdminConfig.save()
	return
#end-def unDeployApplication

# Deploy Application
def deployApplication(AppName):
	appExistLevel = isApplicationExist(AppName)
	if(appExistLevel == "NO"):
		Logger("Deploying applciation "+AppName+" ... ")
		mScopeStr_MapModules = 'WebSphere:cell='+mCell+',node='+mNode+',server='+mServerName
		APP_EAR_Location = "./"+AppName+".ear"
		#AdminApp.install(APP_EAR_Location, '[ -nopreCompileJSPs -distributeApp -nouseMetaDataFromBinary -deployejb -appname '+AppName+' -createMBeansForResources -noreloadEnabled -nodeployws -validateinstall warn -noprocessEmbeddedConfig -filepermission .*\.dll=755#.*\.so=755#.*\.a=755#.*\.sl=755 -noallowDispatchRemoteInclude -noallowServiceRemoteInclude -asyncRequestDispatchType DISABLED -nouseAutoLink -noenableClientModule -clientMode isolated -novalidateSchema -MapModulesToServers [[ TLAppCoreEJB.jar TLAppCoreEJB.jar,META-INF/ejb-jar.xml '+mScopeStr_MapModules+' ][ LMS_EJB_3.1 LmsEjb3.jar,META-INF/ejb-jar.xml '+mScopeStr_MapModules+' ][ lms.war lms.war,WEB-INF/web.xml '+mScopeStr_MapModules+' ]] -MapWebModToVH [[ lms.war lms.war,WEB-INF/web.xml default_host ]]]' )
		
		AdminApp.install(APP_EAR_Location, '[ -nopreCompileJSPs -distributeApp -nouseMetaDataFromBinary -deployejb -appname '+AppName+' -createMBeansForResources -noreloadEnabled -nodeployws -validateinstall warn -noprocessEmbeddedConfig -filepermission .*\.dll=755#.*\.so=755#.*\.a=755#.*\.sl=755 -noallowDispatchRemoteInclude -noallowServiceRemoteInclude -asyncRequestDispatchType DISABLED -nouseAutoLink -noenableClientModule -clientMode isolated -novalidateSchema -MapModulesToServers [[ TLAppCoreEJB.jar TLAppCoreEJB.jar,META-INF/ejb-jar.xml '+mScopeStr_MapModules+' ][ LMS_EJB_3.1 LmsEjb3.jar,META-INF/ejb-jar.xml '+mScopeStr_MapModules+' ][ lms.war lms.war,WEB-INF/web.xml '+mScopeStr_MapModules+' ]] -MapWebModToVH [[ lms.war lms.war,WEB-INF/web.xml default_host ]] -MapSharedLibForMod [[ '+AppName+'  META-INF/application.xml LMS-REF-LIB] [ lms.war lms.war,WEB-INF/web.xml LMS-REF-LIB] ]]' )
		# Saving to master
		AdminConfig.save()

		# setting flag to start application
		StartApplication(AppName)
	else :
		Logger("Can not start deployment of application"+AppName)
#end-def deployApplication()

def getAppStatus(AppName):
	# If objectName is blank, then the application is not running.
	objectName = AdminControl.completeObjectName('type=Application,name=' + AppName + ',*')
	if objectName == "":
		appStatus = 'Stopped'
	else:
		appStatus = 'Running'
	return appStatus
#end-def getAppStatus

def StartApplication(AppName):
	appExistLevel = isApplicationExist(AppName)
	if(appExistLevel == "YES"):
		appStatus = getAppStatus(AppName)
		Logger("Application '"+AppName+"' is "+appStatus)
		if(appStatus == "Stopped"):
			appManager = AdminControl.queryNames(mAppMgrScopeStr)
			#Logger("appManager = "+str(appManager))
			Logger("Starting application, please wait ...")
			AdminControl.invoke(appManager, 'startApplication', AppName)
			Logger("Application started successfully.")
		#end-if
	#end-if
#end-def StartApplication()

def StopApplication(AppName):
	appExistLevel = isApplicationExist(AppName)
	if(appExistLevel == "YES"):
		appStatus = getAppStatus(AppName)
		Logger("Application '"+AppName+"' is "+appStatus)
		if(appStatus == "Running"):
			appManager = AdminControl.queryNames(mAppMgrScopeStr)
			#Logger("appManager = "+str(appManager))
			Logger("Stopping application, please wait ...")
			AdminControl.invoke(appManager, 'stopApplication', AppName)
			Logger("Application stopped successfully.")
		#end-if
	#end-if
#end-def StopApplication()



#-- Execution starts from here -----------------------------------------------------------------------------------

lPropFileName = ScriptName+".properties"
mLMSProperties = loadProps(lPropFileName)
#loadProperties(lPropFileName)


# * Assign the properties to the local variables AND install EAR and Configuration

try:

	Logger("")
	Logger("INTELLECT "+ScriptName+" CONFIGURATION STARTED")
	Logger("")

	# * Getting the cell , node , server and cluster details
	mCell = AdminControl.getCell()
	
	isManagedProcess = mLMSProperties.get('isManagedProcess','false').lower()
	lServerOrProcess = "server"
	#Logger("ManagedProcess  = "+isManagedProcess)
	if isManagedProcess == 'true':
		mNode = mLMSProperties.get('Node_Name','0')
	else:
		mNode = AdminControl.getNode()
	#end-if

	isCluster = mLMSProperties.get('isCluster','false').lower()

	Logger("Environment Details...")
	Logger("ClusterScope : "+isCluster)
	Logger("ManagedProcess : "+isManagedProcess)
	Logger("Cell : "+mCell)
	Logger("Node : "+mNode)

	if isCluster == 'true':
		mClusterName = mLMSProperties.get('Cluster_Name','0')
		mPrefixScopeStr = mClusterName
		mScopeType = 'Cluster'
		mScopeName = mClusterName
		mScopeStr = '/Cell:'+mCell+'/ServerCluster:'+mClusterName
		mScopeId = AdminConfig.getid(mScopeStr)
		mScopeList = ["-cluster", mClusterName]
		mSIBDefaultDS = 'false'
	else:
		mServerName = mLMSProperties.get('Server_Name','0')
		mPrefixScopeStr = mServerName
		mScopeType = 'Server'
		mScopeName = mServerName
		mScopeStr = '/Cell:'+mCell+'/Node:'+mNode+'/Server:'+mServerName
		mScopeId = AdminConfig.getid(mScopeStr)
		mScopeList = ["-node", mNode, "-server", mServerName]
		mSIBDefaultDS = 'true'
		#mAppMgrScopeStr = 'cell='+mCell+',node='+mNode+',type=ApplicationManager,'+lServerOrProcess+'='+mServerName+',*'
		mAppMgrScopeStr = 'cell='+mCell+',node='+mNode+',type=ApplicationManager,process='+mServerName+',*'
	#end if-else

	Logger(mScopeType+" : "+mScopeName)
	Logger("")

	Logger("-" * nbrDashInLine)
	Logger("Scope String : ["+mScopeStr+"]")
	Logger("Scope ID : "+mScopeId)
	Logger("-" * nbrDashInLine)
	Logger("")


	# -----------------------------------------------------

	# -- Check for the arguments if passed and set them to their respective variables

	isArgFound = "false"
	showHelp = 'N'
	doSharedLibConfig = 'N'
	doJMSConfig = 'N'
	removeJMSConfig = 'N'
	doJDBCConfig = 'N'
	doDeployMultiApps = 'N'
	doDeployApp = 'N'
	DeployAppName = "none"
	doUnDeployApp = 'N'
	unDeployAppName = "none"
	doStopApplication = 'N'
	doStartApplication = 'N'
	doRestartApplication = 'N'
	installedAppName = "none"

	Logger("Arguments = "+str(sys.argv))

	if len(sys.argv) >= 1:
		isArgFound = "true"
	#end-if

	# Collecting arguments
	argcnt = 1
	if isArgFound == "true":
		Logger( "Arguments ..." )
		for arg in sys.argv:
			arg = arg.upper()
			if arg == "HELP": showHelp = 'Y'
			if arg == "SHAREDLIB": 
				doSharedLibConfig = 'Y'
				Logger( "	doSharedLibConfig = "+doSharedLibConfig )
			if arg == "JMS": 
				JmsAction = 'CREATE'
				if len(sys.argv) == (argcnt+1): JmsAction = sys.argv[argcnt].upper()
				if JmsAction == 'REMOVE':
					removeJMSConfig = 'Y'
					Logger( "	removeJMSConfig = "+removeJMSConfig )
				else:
					doJMSConfig = 'Y'
					Logger( "	doJMSConfig = "+doJMSConfig )
			if arg == "JDBC": 
				doJDBCConfig = 'Y'
				Logger( "	doJDBCConfig = "+doJDBCConfig )
			if arg == "DEPLOY": 
				if len(sys.argv) == (argcnt+1): 
					doDeployApp = 'Y'
					DeployAppName = sys.argv[argcnt]
					DeployAppName = DeployAppName.replace(".ear", "")
					Logger( "	doDeployApp = "+doDeployApp )
					Logger( "		DeployAppName = "+DeployAppName )
				else:
					doDeployMultiApps = 'Y'
					Logger( "	doDeployMultiApps = "+doDeployMultiApps )
			if arg == "UNDEPLOY": 
				doUnDeployApp = 'Y'
				if len(sys.argv) == (argcnt+1): 
					unDeployAppName = sys.argv[argcnt]
					unDeployAppName = unDeployAppName.replace(".ear", "")
				Logger( "	doUnDeployApp = "+doUnDeployApp )
				Logger( "		unDeployAppName = "+unDeployAppName )
			if arg == "STOP": 
				doStopApplication = 'Y'
				if len(sys.argv) == (argcnt+1): 
					installedAppName = sys.argv[argcnt]
					installedAppName = installedAppName.replace(".ear", "")
				Logger( "	doStopApplication = "+doStopApplication )
				Logger( "		installedAppName = "+installedAppName )
			if arg == "START": 
				doStartApplication = 'Y'
				if len(sys.argv) == (argcnt+1): 
					installedAppName = sys.argv[argcnt]
					installedAppName = installedAppName.replace(".ear", "")
				Logger( "	doStartApplication = "+doStartApplication )
				Logger( "		installedAppName = "+installedAppName )
			if arg == "RESTART": 
				doRestartApplication = 'Y'
				if len(sys.argv) == (argcnt+1): 
					installedAppName = sys.argv[argcnt]
					installedAppName = installedAppName.replace(".ear", "")
				Logger( "	doRestartApplication = "+doRestartApplication )
				Logger( "		installedAppName = "+installedAppName )
			argcnt = argcnt + 1
		#end-for

		Logger("-" * nbrDashInLine)
	else:
		Logger("")
		showHelp = "Y"
		#Logger("None of the arguments are passed at commad line, Please confirm below operations....")
		# if arguments are not passed at command line then get the confirmation to do following operations.
		#doSharedLibConfig = getInput('Do you want to create Shared Libraries (y/n)?[n] ').upper()
		#doJMSConfig = getInput('Do you want to create JMS Configuration (y/n)?[n] ').upper()
		#doJDBCConfig = getInput('Do you want to create JDBC Configuration (y/n)?[n] ').upper()
		#doDeployApp = getInput('Do you want to deploy applications (y/n)?[n] ').upper()
	#end-if-else
	Logger("")
	Logger("-" * nbrDashInLine)
	Logger("")
	# -----------------------------------------------------
	
	# -----------------------------------------------------
	flgSave = 'false'

	if showHelp != "Y":
		if doSharedLibConfig == "Y":
			CreateSharedLibraries()
			SaveConfiguration()

		if doJMSConfig == "Y":
			CreateJMS_Config()
			SaveConfiguration()
		
		if doJDBCConfig == "Y":
			CreateJDBC_Config()
			SaveConfiguration()
		
		if doDeployApp == "Y":
			unDeployApplication(DeployAppName)
			deployApplication(DeployAppName)

		if doUnDeployApp == "Y":
			unDeployApplication(unDeployAppName)

		if doStopApplication == "Y":
			StopApplication(installedAppName)

		if doStartApplication == "Y":
			StartApplication(installedAppName)

		if doRestartApplication == "Y":
			StopApplication(installedAppName)
			StartApplication(installedAppName)
	else:
		usage()
	# end If-else
	# -----------------------------------------------------

except Exception, value:
	print Exception.toString(), value
	#Logger("Unexpected error: "+(sys.exc_info()[0]).toString()+" "+sys.exc_info()[1])
	#Logger("Unexpected error: "+Exception.toString()+" "+value)
		

Logger("")
Logger("-" * nbrDashInLine)
Logger("-- End of LMS WAS Configuration --")	
Logger("-" * nbrDashInLine)
Logger("")

handle.close()
	